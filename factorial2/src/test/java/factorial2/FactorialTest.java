package factorial2;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
public class FactorialTest {
	
	private Factorial factorial;
	
	@Before
	public void start(){
		factorial=new Factorial();
	}
	
	//test to check that factorial of zero is one
	@Test
	public void test1(){
		
		int expectedValue=1;
		int receivedValue=factorial.compute(0);
		assertEquals(expectedValue,receivedValue);
		
	}
	//test to check that factorial of one is one
	@Test
	public void test2(){
		
		int expectedValue=1;
		int receivedValue=factorial.compute(1);
		assertEquals(expectedValue,receivedValue);
		
	}
	public void test3(){
		
		int expectedValue=2;
		int receivedValue=factorial.compute(2);
		assertEquals(expectedValue,receivedValue);
		
	}
	public void test4(){
		
		int expectedValue=6;
		int receivedValue=factorial.compute(3);
		assertEquals(expectedValue,receivedValue);
		
	}
	public void test5(){
		
		int expectedValue=120;
		int receivedValue=factorial.compute(5);
		assertEquals(expectedValue,receivedValue);
		
	}
	


}
